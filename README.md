<p align="center">
    <h1 align="center">Yii 2 Тестовое задание</h1>
    <br>
</p>

Установка с Docker-compose
------------
    #Клонируем конфиг
    cp .env.example .env
    
    #Обновить зависимости
    docker-compose run --rm php composer update --prefer-dist
    
    #Установить зависимости
    docker-compose run --rm php composer install
    
    #Запустить контейнер
    docker-compose up -d
    
    #Запустить миграции модуля Orders (создадутся таблицы (без данных), пропишутся ключи и индексы)
    docker-compose run --rm php yii migrate --migrationPath @app/modules/orders/migrations/
    
    #Приложение доступно по следующему адресу:
    http://127.0.0.1:8000/

**NOTES:** 
- Minimum required Docker engine version `17.04` for development (see [Performance tuning for volume mounts](https://docs.docker.com/docker-for-mac/osxfs-caching/))
- The default configuration uses a host-volume in your home directory `.docker-composer` for composer caches
