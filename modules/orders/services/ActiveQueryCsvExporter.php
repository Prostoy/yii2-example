<?php
/*
 *
 */
declare(strict_types=1);

namespace app\modules\orders\services;

use app\modules\orders\mappers\OrdersArrayMapper;
use RuntimeException;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Exception;

/**
 * Class ActiveQueryCsvExporter
 */
class ActiveQueryCsvExporter
{
    protected const DEFAULT_FILE_NAME = 'export.csv';

    /**
     * @var OrdersArrayMapper Маппер для массиовов
     */
    public OrdersArrayMapper $mapper;

    /**
     * @var int Размер выборки из БД
     */
    public int $batchSize = 1000;

    /**
     * @var string Адрес ресурса для записи
     */
    public string $resourceAddress;

    /**
     * @var string Режим записи
     */
    public string $writeMode = 'wb';

    /**
     * @var string Разделитель
     */
    public string $delimiter = ',';

    /**
     * @var bool Флаг для записи заголовка из ключей массива
     */
    public bool $writeHeader = true;

    /**
     * @var bool Флаг для записи BOM (для чтения результата из Excel)
     */
    public bool $writeBom = true;

    /**
     * @var ActiveQuery Конструктор запросов, на основе которого будет собран результат
     */
    protected ActiveQuery $exportQuery;

    /**
     * ActiveQueryCsvExporter constructor.
     * @param string|null $resourceAddress
     */
    public function __construct(string $resourceAddress = null)
    {
        if($resourceAddress){
            $this->setResourceAddress($resourceAddress);
        }
    }

    /**
     * Отправляет заголовки для загрузки файла
     *
     * @param string $filename
     *
     * @return $this
     */
    protected function sendStream(string $filename): self
    {
        Yii::$app->response->setDownloadHeaders(
            $filename,
            ['mimeType' => 'text/csv']
        )->send();

        return $this;
    }

    /**
     * Экспортирует результаты выборки из $this->exportQuery в $this->resourceAddress
     *
     * @throws RuntimeException Если не удалось записать строку или открыть ресурс на запись
     * @throws Exception Если не удалось создать отдельное подключение к БД
     */
    public function export(): void
    {
        $resource = fopen($this->resourceAddress, $this->writeMode);
        if (!$resource) {
            throw new RuntimeException('Не удалось открыть поток ' . $this->resourceAddress);
        }

        if ($this->writeBom) {
            $writeResult = fwrite($resource, (chr(0xEF) . chr(0xBB) . chr(0xBF)));
            if (!$writeResult) {
                throw new RuntimeException('Ошибка записи BOM');
            }

            $this->writeBom = false;
        }

        $line = 1;
        $writeHeader = $this->writeHeader;
        $this->exportQuery->asArray();
        foreach ($this->exportQuery->batch($this->batchSize) as $collection) {
            $collection = $this->mapper->map($collection);

            if ($writeHeader) {
                $writeResult = fputcsv(
                    $resource,
                    array_keys(current($collection)),
                    $this->delimiter
                );
                if (!$writeResult) {
                    throw new RuntimeException('Ошибка записи заголовка');
                }

                $writeHeader = $this->writeHeader = false;
                $line++;
            }
            foreach ($collection as $row) {


                $writeResult = fputcsv($resource, $row, $this->delimiter);
                if (!$writeResult) {
                    throw new RuntimeException('Ошибка записи строки ' . $line);
                }

                $line++;
            }
        }

        fclose($resource);
    }

    /**
     * @param OrdersArrayMapper $mapper
     *
     * @return $this
     */
    public function setMapper(OrdersArrayMapper $mapper): ActiveQueryCsvExporter
    {
        $this->mapper = $mapper;
        return $this;
    }

    /**
     * @param ActiveQuery $exportQuery
     *
     * @return $this
     */
    public function setExportQuery(ActiveQuery $exportQuery): ActiveQueryCsvExporter
    {
        $this->exportQuery = $exportQuery;
        return $this;
    }

    /**
     * @param string $resourceAddress Адрес ресурса для записи
     * @param string|null $resultFileName Название файла (при передаче потока в браузер)
     * @return $this
     */
    public function setResourceAddress(string $resourceAddress, string $resultFileName = null): ActiveQueryCsvExporter
    {
        $this->resourceAddress = $resourceAddress;
        $resultFileName = $resultFileName ?? self::DEFAULT_FILE_NAME;

        if($resourceAddress === 'php://output')
        {
            $this->sendStream($resultFileName);
        }

        return $this;
    }
}