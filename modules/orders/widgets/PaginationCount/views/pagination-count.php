<?php
/**
 * @var PaginationCount $paginationCount
 *
 */

use app\modules\orders\widgets\PaginationCount\PaginationCount;
use yii\helpers\Url;
?>

<div style="float: right; min-height: 75px;">
    <?php $pages = $paginationCount->pagination; ?>
    <?php if ($pages->totalCount > $pages->limit): ?>
        <?= Yii::t('modules/orders/default', 'current') ?>: <?= $pages->offset + $pages->limit?><br>
    <?php endif; ?>

    <?php if ($pages->totalCount > 0): ?>
        <?= Yii::t('modules/orders/default', 'total') ?>: <?= $pages->totalCount ?><br>
        <a href="<?= Url::toRoute(array_merge(['export'],
            $paginationCount->ordersSearch->getUrlParams())) ?>"
           target="_blank"><?= Yii::t('modules/orders/default', 'Save result') ?></a>
    <?php endif; ?>
</div>