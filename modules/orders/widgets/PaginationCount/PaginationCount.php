<?php

namespace app\modules\orders\widgets\PaginationCount;


use app\modules\orders\models\OrdersSearch;
use yii\base\Widget;
use yii\data\Pagination;

class PaginationCount extends Widget
{
    /**
     * @var Pagination Пагинация
     */
    public Pagination $pagination;

    /**
     * @var OrdersSearch Поисковая модель заказов
     */
    public OrdersSearch $ordersSearch;

    /**
     * Инициация виджета
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string Выполнение виджета
     */
    public function run(): string
    {
        return $this->render('pagination-count', ['paginationCount' => $this]);
    }
}