<?php
/**
 *
 */
declare(strict_types=1);

namespace app\modules\orders\widgets\FilterDropdown;


use app\modules\orders\models\OrdersSearch;
use yii\base\Widget;

/**
 * Class FilterDropdown
 *
 * @package app\modules\orders\widgets\FilterDropdown
 */
class FilterDropdown extends Widget
{
    /**
     * @var string Ключ параметра
     */
    public string $paramName;

    /**
     * @var string Заголовок параметра
     */
    public string $title;

    /**
     * @var string|null Текущее значение
     */
    public ?string $currentValue;

    /**
     * @var string Заголовок при пустом значении
     */
    public string $nullTitle;

    /**
     * @var string|null Ключ заголовка
     */
    public ?string $titleKey = null;

    /**
     * @var string|null Ключ подзаголовка
     */
    public ?string $subtitleKey = null;

    /**
     * @var array Массив значений
     */
    public array $variants;

    /**
     * @var OrdersSearch Поисковая модель заказов
     */
    public OrdersSearch $ordersSearch;

    /**
     * Инициация виджета
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string Выполнение виджета
     */
    public function run(): string
    {
        return $this->render('dropdown', ['dropdown' => $this]);
    }
}