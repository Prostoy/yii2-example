<?php
/** @var FilterDropdown $dropdown */

use app\modules\orders\widgets\FilterDropdown\FilterDropdown;
use yii\helpers\Url;

?>

<div class="dropdown">
    <button class="btn btn-th btn-default dropdown-toggle"
            type="button"
            id="dropdownMenu1"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="true">
        <a><?= Yii::t('modules/orders/default', $dropdown->title) ?></a>
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu"
        aria-labelledby="dropdownMenu1">
        <li <?= $dropdown->currentValue === null ? 'class="active"' : '' ?>>
            <a href="<?= Url::toRoute(array_merge([''],
                $dropdown->ordersSearch->getUrlParams($dropdown->paramName))) ?>"><?= $dropdown->nullTitle ?>
            </a>
        </li>

        <?php foreach ($dropdown->variants as $key => $variant): ?>
            <li <?= $dropdown->currentValue === $key ? 'class="active"' : '' ?>>
                <a href="<?= Url::toRoute(array_merge([''],
                    $dropdown->ordersSearch->getUrlParams($dropdown->paramName, $key))) ?>">
                    <?php if ($dropdown->subtitleKey): ?>
                        <span class="label-id"><?= $variant[$dropdown->subtitleKey] ?></span>
                    <?php endif; ?>

                    <?= Yii::t('modules/orders/default', $dropdown->titleKey ? $variant[$dropdown->titleKey] : $variant) ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>