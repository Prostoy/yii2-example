<?php
/**
 *
 */
declare(strict_types=1);

namespace app\modules\orders;

use Yii;

/**
 * Class Module
 * @package app\modules\orders
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'app\modules\orders\controllers';

    /**
     * @var string
     */
    public $layout = 'main';

    public function init()
    {
        parent::init();
        $this->registerTranslations();

    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/orders/*'] = [
            'class'          => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath'       => '@app/modules/orders/messages',
            'fileMap'        => [
                'modules/orders/default' => 'default.php',
            ],
        ];
    }
}
