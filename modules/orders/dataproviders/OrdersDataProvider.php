<?php
/**
 *
 */
declare(strict_types=1);

namespace app\modules\orders\dataproviders;

use app\modules\orders\mappers\OrdersArrayMapper;
use InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQueryInterface;

/**
 * Class OrdersDataProvider
 *
 * @package app\modules\orders\dataproviders
 */
class OrdersDataProvider extends ActiveDataProvider
{
    /**
     * @var OrdersArrayMapper Маппер для заказов
     */
    public OrdersArrayMapper $mapper;

    /**
     * Подготовка массивов заказов.
     * Прогоняет через маппер
     *
     * @return array
     * @throws InvalidConfigException
     */
    protected function prepareModels(): array
    {
        if(!($this->query instanceof ActiveQueryInterface)){
            throw new InvalidArgumentException('The "query" property must be an instance of a class that implements the ActiveQueryInterface.');
        }

        $this->query->asArray();

        return $this->mapper->map(parent::prepareModels());
    }
}