<?php /** @noinspection ArgumentEqualsDefaultValueInspection */
/** @var OrdersDataProvider $ordersDataProvider */
/** @var OrdersSearch $ordersSearch */
/** @var array $statuses */
/** @var array $mods */

/** @var array $services */

use app\modules\orders\dataproviders\OrdersDataProvider;
use app\modules\orders\models\Orders;
use app\modules\orders\models\OrdersSearch;
use app\modules\orders\widgets\FilterDropdown\FilterDropdown;
use app\modules\orders\widgets\PaginationCount\PaginationCount;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>


<div class="container-fluid">
    <ul class="nav nav-tabs p-b">
        <li <?= $ordersSearch->status === null ? 'class="active"' : '' ?>>
            <a href="<?= Url::toRoute(array_merge([''], $ordersSearch->getUrlParams('status'))) ?>">
                <?= Yii::t('modules/orders/default', 'All orders') ?>
            </a>
        </li>

        <?php foreach ($statuses as $key => $status): ?>
            <li <?= $ordersSearch->status === $key ? 'class="active"' : '' ?>>
                <a href="<?= Url::toRoute(array_merge([''],
                    $ordersSearch->getUrlParams('status', $key))) ?>"><?= Yii::t('modules/orders/default', $status) ?></a>
            </li>
        <?php endforeach; ?>


        <li class="pull-right custom-search">
            <form class="form-inline"
                  action="<?= Url::toRoute('') ?>"
                  method="get">
                <div class="input-group">
                    <input type="text"
                           name="search"
                           class="form-control"
                           value="<?= $ordersSearch->search ?>"
                           placeholder="<?= Yii::t('modules/orders/default', 'Search orders') ?>">
                    <span class="input-group-btn search-select-wrap">

                        <?php foreach ($ordersSearch->getUrlParams('search', '') as $param => $value): ?>
                            <?php if (!is_null($value)): ?>
                                <input type="hidden"
                                       name="<?= $param ?>"
                                       value="<?= $value ?>">
                            <?php endif; ?>
                        <?php endforeach; ?>

            <select class="form-control search-select"
                    name="search_type">
                <?php foreach ($ordersSearch::SEARCH_TYPES as $key => $type): ?>
                    <option value="<?= $key ?>" <?= $ordersSearch->search_type === $key ? 'selected="true"' : '' ?> >
                        <?= Yii::t('modules/orders/default', $type) ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <button type="submit"
                    class="btn btn-default"><span class="glyphicon glyphicon-search"
                                                  aria-hidden="true"></span></button>
            </span>
                </div>
            </form>
        </li>
    </ul>
    <table class="table order-table">
        <thead>
        <tr>
            <th><?= Yii::t('modules/orders/default', 'ID') ?></th>
            <th><?= Yii::t('modules/orders/default', 'User') ?></th>
            <th><?= Yii::t('modules/orders/default', 'Link') ?></th>
            <th><?= Yii::t('modules/orders/default', 'Quantity') ?></th>
            <th class="dropdown-th">
                <?= FilterDropdown::widget([
                    'paramName' => 'service',
                    'title' => 'Service',
                    'currentValue' => $ordersSearch->service,
                    'nullTitle' => Yii::t('modules/orders/default', 'All') . '(' . Orders::total() . ')',
                    'titleKey' => 'name',
                    'subtitleKey' => 'orders_count',
                    'variants' => $services,
                    'ordersSearch' => $ordersSearch,
                ]) ?>
            </th>
            <th><?= Yii::t('modules/orders/default', 'Status') ?></th>
            <th class="dropdown-th">
                <?= FilterDropdown::widget([
                    'paramName' => 'mode',
                    'title' => 'Mode',
                    'currentValue' => $ordersSearch->mode,
                    'nullTitle' => Yii::t('modules/orders/default', 'All'),
                    'variants' => $mods,
                    'ordersSearch' => $ordersSearch,
                ]) ?>

            </th>
            <th><?= Yii::t('modules/orders/default', 'Created') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($ordersDataProvider->getModels() as $order): ?>
            <tr>
                <td><?= $order['id'] ?></td>
                <td><?= $order['user_name'] ?></td>
                <td class="link"><?= $order['link'] ?></td>
                <td><?= $order['quantity'] ?></td>

                <td class="service">
                    <span class="label-id"><?= $order['service']['orders_count'] ?>
                    </span><?= Yii::t('modules/orders/default',
                        $order['service']['name']) ?>
                </td>
                <td><?= Yii::t('modules/orders/default', $order['status']) ?></td>
                <td><?= Yii::t('modules/orders/default', $order['mode']) ?></td>
                <td>
                    <span class="nowrap"><?= $order['created_at']['date'] ?></span>
                    <span class="nowrap"><?= $order['created_at']['time'] ?></span>
                </td>
            </tr>
        <?php endforeach; ?>


        </tbody>
    </table>
    <?= LinkPager::widget([
        'pagination' => $ordersDataProvider->getPagination(),
    ]) ?>

    <?= PaginationCount::widget([
        'pagination' => $ordersDataProvider->getPagination(),
        'ordersSearch' => $ordersSearch,
    ]) ?>






</div>