<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
declare(strict_types=1);

namespace app\modules\orders\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@app/modules/orders/assets';

    /**
     * @var string[]
     */
    public $css = [
        'css/bootstrap.min.css',
        'css/custom.css',
    ];

    /**
     * @var string[]
     */
    public $js = [
        'js/jquery.min.js',
        'js/bootstrap.min.js'
    ];

    /**
     * @var string[][]
     */
    public $publishOptions = [
        'only' => [
            'fonts/*',
            'css/*',
            'js/*',
        ]
    ];
}
