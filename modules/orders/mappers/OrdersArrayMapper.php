<?php
/**
 *
 */
declare(strict_types=1);

namespace app\modules\orders\mappers;

use app\modules\orders\models\Orders;
use DateTime;
use Throwable;

/**
 * Class OrdersArrayMapper
 *
 * @package app\modules\orders\mappers
 */
class OrdersArrayMapper
{
    public const DATE_FORMAT = 'Y-m-d';
    public const TIME_FORMAT = 'H:i:s';

    /**
     * @var bool Подготовка массива для вывода
     */
    protected bool $forView = false;

    /**
     * @var bool Подготовка массива для экспорта в csv
     */
    protected bool $forCsv = false;

    /**
     * @var array Коллекция услуг
     */
    protected array $services;

    /**
     * OrdersArrayMapper constructor.
     *
     * @param array $services Коллекция услуг, индексированных по id
     */
    public function __construct(array $services)
    {
        $this->services = $services;
    }

    /**
     * Обход и форматирование массива заказов
     *
     * @param array $orders Коллекция заказов
     *
     * @return array Коллекция заказов
     */
    public function map(array $orders): array
    {
        foreach ($orders as $key => $order) {
            $order = $this->mergeService($order);

            if ($this->forView) {
                $order = $this->prepareForView($order);
            }

            if ($this->forCsv) {
                $order = $this->prepareForCsv($order);
            }

            $orders[$key] = $order;
        }

        return $orders;
    }

    /**
     * @param bool $forView
     *
     * @return self
     */
    public function setViewMode(bool $forView = true): self
    {
        $this->forView = $forView;
        return $this;
    }

    /**
     * @param bool $forCsv
     *
     * @return self
     */
    public function setCsvMode(bool $forCsv = true): self
    {
        $this->forCsv = $forCsv;
        $this->forView = true;
        return $this;
    }

    /**
     * Добавляет поля сервиса к заказу
     *
     * @param array $order Заказ
     *
     * @return array Заказ
     */
    protected function mergeService(array $order): array
    {
        $order['service'] = $this->services[$order['service_id']] ?? [];
        return $order;
    }

    /**
     * Подготовить заказ для экспорта в csv
     *
     * @param array $order
     *
     * @return array
     */
    protected function prepareForCsv(array $order): array
    {
        return [
            'ID' => $order['id'],
            'User' => $order['user_name'],
            'Link' => $order['link'],
            'Quantity' => $order['quantity'],
            'Service' => '['.$order['service']['orders_count'].']'.$order['service']['name'],
            'Status' => $order['status'],
            'Mode' => $order['mode'],
            'Created' => $order['created_at']['date'] . ' ' . $order['created_at']['time'],
        ];
    }

    /**
     * Подготовить заказ для вывода в браузер
     *
     * @param array $order Заказ
     *
     * @return array Заказ
     */
    protected function prepareForView(array $order): array
    {
        $order = self::formatDateTime($order);
        $order = self::formatMode($order);
        $order = self::formatStatus($order);
        return $order;
    }

    /**
     * Форматирует даты в заказе
     *
     * @param array $order Заказ
     *
     * @return array Заказ
     */
    protected static function formatDateTime(array $order): array
    {
        try {
            $dateTime = (new DateTime())->setTimestamp((int)$order['created_at']);

            $order['created_at'] = [
                'timestamp' => $order['created_at'],
                'date' => $dateTime->format(self::DATE_FORMAT),
                'time' => $dateTime->format(self::TIME_FORMAT)
            ];
        } catch (Throwable $e) {
            $order['created_at'] = [
                'timestamp' => $order['created_at'],
                'date' => '',
                'time' => '',
            ];
        }

        return $order;
    }

    /**
     * Форматирует режим заказа
     *
     * @param array $order
     *
     * @return array
     */
    protected static function formatMode(array $order): array
    {
        $order['mode'] = Orders::MODS[$order['mode']];

        return $order;
    }

    /**
     * Форматирует статус заказа
     *
     * @param array $order
     *
     * @return array
     */
    protected static function formatStatus(array $order): array
    {
        $order['status'] = Orders::STATUSES[$order['status']];

        return $order;
    }
}