<?php
/**
 *
 */

namespace app\modules\orders\models;


use app\modules\orders\dataproviders\OrdersDataProvider;
use app\modules\orders\mappers\OrdersArrayMapper;
use app\modules\orders\services\ActiveQueryCsvExporter;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Class OrdersSearch
 * Поисковая модель заказов
 *
 * @package app\modules\orders\models
 */
class OrdersSearch extends Model
{
    public const SEARCH_BY_ORDER_ID = 0;
    public const SEARCH_BY_LINK = 1;
    public const SEARCH_BY_USERNAME = 2;

    public const SEARCH_TYPES = [
        self::SEARCH_BY_ORDER_ID => 'Order ID',
        self::SEARCH_BY_LINK => 'Link',
        self::SEARCH_BY_USERNAME => 'Username'
    ];

    public const STATUS = 'status';
    public const MODE = 'mode';
    public const SERVICE = 'service';
    public const SEARCH_TYPE = 'search_type';
    public const SEARCH = 'search';

    /**
     * Статус заказа
     *
     * @var int|null
     */
    public ?int $status = null;

    /**
     * Идентификатор сервиса
     *
     * @var int|null
     */
    public ?int $service = null;

    /**
     * Идентификатор режима заказа
     *
     * @var int|null
     */
    public ?int $mode = null;

    /**
     * Тип поиска
     *
     * @var int|null
     */
    public ?int $search_type = null;

    /**
     * Значение поиска
     *
     * @var string|null
     */
    public ?string $search = null;

    /**
     * Конструктор запросов
     *
     * @var ActiveQuery
     */
    protected ActiveQuery $query;

    /**
     * Размер страницы
     *
     * @var int
     */
    public int $pageSize = 100;

    /**
     * Время кеширования результатов выборок
     *
     * @var int
     */
    public int $cacheTime = 60;


    /**
     * Правила валидации
     *
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [
                [
                    self::STATUS,
                    self::SERVICE,
                    self::MODE,
                    self::SEARCH_TYPE
                ],
                'integer'
            ],
            [
                self::STATUS,
                'in',
                'range' => array_keys(Orders::STATUSES)
            ],
            [
                self::MODE,
                'in',
                'range' => array_keys(Orders::MODS)
            ],
            [
                self::SEARCH_TYPE,
                'in',
                'range' => array_keys(self::SEARCH_TYPES)
            ],
            [
                self::SERVICE,
                'in',
                'range' => array_keys(ServicesSearch::getCountedByOrders())
            ],
            [
                [self::SEARCH],
                'string',
                'when' => function (self $model) {
                    return in_array($model->search_type, [
                        self::SEARCH_BY_USERNAME,
                        self::SEARCH_BY_LINK
                    ]);
                }
            ],
            [
                [self::SEARCH],
                'integer',
                'when' => function (self $model) {
                    return $model->search_type == self::SEARCH_BY_ORDER_ID;
                }
            ]
        ];
    }

    /**
     * Названия атрибутов
     *
     * @return array
     */
    public function attributeLabels(): array
    {
        return [
            self::STATUS => Yii::t('modules/orders/default', 'Status'),
            self::SERVICE => Yii::t('modules/orders/default', 'Service'),
            self::MODE => Yii::t('modules/orders/default', 'Mode'),
            self::SEARCH_TYPE => Yii::t('modules/orders/default', 'Search by'),
            self::SEARCH => Yii::t('modules/orders/default', 'Search'),
        ];
    }

    /**
     * Поиск заказов по параметрам
     *
     * @param array $params
     *
     * @return $this
     */
    public function search(array $params): self
    {
        $this->query = Orders::find()
                             ->leftJoin('users', 'users.id = orders.user_id')
                             ->select([
                                 'orders.id',
                                 'orders.user_id',
                                 'orders.link',
                                 'orders.quantity',
                                 'orders.service_id',
                                 'orders.status',
                                 'orders.created_at',
                                 'orders.mode',
                                 "CONCAT(users.first_name, ' ', users.last_name) as user_name"
                             ])
                             ->orderBy('id DESC')
                             ->asArray();

        $this->load($params, '');

        if (!$this->validate()) {
            $this->query->where('0=1');
        }

        $this->filter($this->query);

        return $this;
    }

    /**
     * Добавление фильтрации в конструктор запросов согласно параметрам поисковой модели
     *
     * @param ActiveQuery $query
     */
    public function filter(ActiveQuery $query): void
    {
        if ($this->status !== null) {
            $query->andFilterWhere([
                '=',
                self::STATUS,
                $this->status
            ]);
        }

        if ($this->service !== null) {
            $query->andFilterWhere([
                '=',
                'service_id',
                $this->service
            ]);
        }

        if ($this->mode !== null) {
            $query->andFilterWhere([
                '=',
                self::MODE,
                $this->mode
            ]);
        }

        if ($this->search) {
            switch ($this->search_type) {
                case null:
                case self::SEARCH_BY_ORDER_ID:
                    $query->andFilterWhere([
                        '=',
                        'orders.id',
                        (int)$this->search
                    ]);
                    break;
                case self::SEARCH_BY_LINK:
                    $query->andFilterWhere([
                        'like',
                        'link',
                        $this->search
                    ]);
                    break;
                case self::SEARCH_BY_USERNAME:
                    $query->andHaving([
                        'like',
                        'user_name',
                        $this->search
                    ]);
                    break;
            }
        }
    }

    /**
     * Возвращает параметры URL для поиска с использованием модели
     *
     * @param string|null $changedField
     * @param null $value
     *
     * @return array
     */
    public function getUrlParams(
        string $changedField = null,
        $value = null
    ): array
    {
        $urlParams = [
            self::STATUS => $this->status,
            self::MODE => $this->mode,
            self::SERVICE => $this->service,
            self::SEARCH_TYPE => $this->search_type,
            self::SEARCH => $this->search
        ];

        if ($changedField) {
            $urlParams[$changedField] = $value;
        }

        if ($changedField === self::STATUS) {
            $urlParams[self::MODE] = null;
            $urlParams[self::SERVICE] = null;
        }

        if ($changedField === self::SEARCH) {
            $urlParams[self::MODE] = null;
            $urlParams[self::SERVICE] = null;
            $urlParams[self::SEARCH_TYPE] = null;
            $urlParams[self::SEARCH] = null;
        }

        return $urlParams;
    }

    /**
     * Возвращает OrdersDataProvider для вывода результатов с пагинацией
     *
     * @return OrdersDataProvider
     */
    public function getDataProvider(): OrdersDataProvider
    {
        return new OrdersDataProvider(
            [
                'query' => $this->query->cache($this->cacheTime),
                'mapper' => (new OrdersArrayMapper(ServicesSearch::getCountedByOrders()))
                    ->setViewMode(),
                'pagination' => [
                    'pageSize' => $this->pageSize,
                ],
            ]
        );
    }

    /**
     * Возвращает конструктор запросов
     *
     * @return ActiveQuery
     */
    public function getQuery(): ActiveQuery
    {
        return $this->query;
    }

    /**
     * Возвращает подготовленный сервис для экспорта в csv
     *
     * @return ActiveQueryCsvExporter
     */
    public function getCsvExporter(): ActiveQueryCsvExporter
    {
        $mapper = (new OrdersArrayMapper(ServicesSearch::getCountedByOrders()))
            ->setCsvMode();

        return (new ActiveQueryCsvExporter())
            ->setExportQuery($this->query)
            ->setMapper($mapper)
            ->setExportQuery($this->query);
    }
}