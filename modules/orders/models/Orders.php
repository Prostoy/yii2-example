<?php
declare(strict_types=1);

namespace app\modules\orders\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Orders
 * Модель таблицы "Заказы" (orders)
 *
 * @package app\modules\orders\models
 */
class Orders extends ActiveRecord
{
    public const MODE_MANUAL = 0;
    public const MODE_AUTH = 1;

    public const MODS = [
        self::MODE_MANUAL => 'Manual',
        self::MODE_AUTH => 'Auto'
    ];

    public const STATUS_PENDING = 0;
    public const STATUS_IN = 1;
    public const STATUS_COMPLETED = 2;
    public const STATUS_CANCELED = 3;
    public const STATUS_FAIL = 4;

    public const STATUSES = [
        self::STATUS_PENDING => 'Pending',
        self::STATUS_IN => 'In progress',
        self::STATUS_COMPLETED => 'Completed',
        self::STATUS_CANCELED => 'Canceled',
        self::STATUS_FAIL => 'Fail'
    ];

    /**
     * @var int Идентификатор заказа
     */
    public int $id;

    /**
     * @var int Идентификатор пользователя
     */
    public int $user_id;

    /**
     * @var string Ссылка
     */
    public string $link;

    /**
     * @var int Количество
     */
    public int $quantity;

    /**
     * @var int Идентификатор сервиса
     */
    public int $service_id;

    /**
     * @var int Идентификатор статуса
     */
    public int $status;

    /**
     * @var int ДатаВремя создания заказа
     */
    public int $created_at;

    /**
     * @var int Режим заказа
     */
    public int $mode;

    /**
     * @var string ИмяФамилия пользователя
     */
    public string $user_name;

    /**
     * @var string Название сервиса
     */
    public string $service_name;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'orders';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [
                [
                    'user_id',
                    'link',
                    'quantity',
                    'service_id',
                    'status',
                    'created_at',
                    'mode'
                ],
                'required'
            ],
            [
                [
                    'user_id',
                    'quantity',
                    'service_id',
                    'status',
                    'created_at',
                    'mode'
                ],
                'integer'
            ],
            [
                ['link'],
                'string',
                'max' => 300
            ],
            [
                ['username'],
                'string'
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('modules/orders/default', 'ID'),
            'user_id' => Yii::t('modules/orders/default', 'User ID'),
            'link' => Yii::t('modules/orders/default', 'Link'),
            'quantity' => Yii::t('modules/orders/default', 'Quantity'),
            'service_id' => Yii::t('modules/orders/default', 'Service ID'),
            'status' => Yii::t('modules/orders/default', 'Status'),
            'created_at' => Yii::t('modules/orders/default', 'Created At'),
            'mode' => Yii::t('modules/orders/default', 'Mode')
        ];
    }

    /**
     * Связь с сервисом
     *
     * @return ActiveQuery
     */
    public function getService(): ActiveQuery
    {
        return $this->hasOne(Services::class, ['id' => 'service_id']);
    }

    /**
     * Связь с пользователем
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * Общее количество заказов
     *
     * @return int
     */
    public static function total(): int
    {
        return (int)static::find()->cache(100)->count();
    }
}
