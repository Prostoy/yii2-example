<?php
/**
 *
 */

namespace app\modules\orders\models;

use yii\base\Model;

/**
 * Class ServicesSearch
 * Поисковая модель услуг
 *
 * @package app\modules\orders\models
 */
class ServicesSearch extends Model
{
    /**
     * @return array
     */
    public static function getCountedByOrders(): array
    {
        return Services::find()
                       ->leftJoin('orders', 'orders.service_id = services.id')
                       ->select([
                           'services.id',
                           'services.name',
                           'orders_count' => 'count(services.id)'
                       ])
                       ->groupBy(['id'])
                       ->orderBy('orders_count DESC')
                       ->indexBy('id')
                       ->asArray()->cache(100)->all();
    }
}