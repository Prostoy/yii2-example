<?php

namespace app\modules\orders\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Users
 * Модель таблицы "Пользователи" (users)
 *
 * @package app\modules\orders\models
 */
class Users extends ActiveRecord
{
    /**
     * @var int Идентификатор пользователя
     */
    public int $id;

    /**
     * @var string Имя пользователя
     */
    public string $first_name;

    /**
     * @var string Фамилия пользователя
     */
    public string $last_name;

    /**
     * @var Orders[]
     */
    public array $orders;

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'users';
    }

    /**
     * Правила валидации
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('modules/orders/default','ID'),
            'first_name' => Yii::t('modules/orders/default','First Name'),
            'last_name' => Yii::t('modules/orders/default','Last Name')
        ];
    }

    /**
     * Связь пользователя с заказами
     *
     * @return ActiveQuery
     */
    public function getOrders(): ActiveQuery
    {
        return $this->hasMany(Orders::class, ['user_id' => 'user_id']);
    }
}
