<?php
declare(strict_types=1);
/**
 *
 */

namespace app\modules\orders\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Services
 * Модель таблицы "Услуги" (services)
 *
 * @package app\modules\orders\models
 */
class Services extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'services';
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [
                ['name'],
                'required'
            ],
            [
                ['name'],
                'string',
                'max' => 300
            ],
        ];
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id'           => Yii::t('modules/orders/default','ID'),
            'name'         => Yii::t('modules/orders/default','Name'),
        ];
    }
}
