<?php

use yii\db\Migration;

class m210210_173714_ordersDataInsert extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__.'/test_db_data.sql'));
    }

    public function safeDown()
    {
        $this->truncateTable('services');
        $this->truncateTable('users');
        $this->truncateTable('orders');
    }
}
