<?php

use yii\db\Migration;

class m210210_173514_ordersScheme extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__.'/test_db_structure.sql'));
    }

    public function safeDown()
    {
        $this->dropTable('services');
        $this->dropTable('users');
        $this->dropTable('orders');
    }
}
