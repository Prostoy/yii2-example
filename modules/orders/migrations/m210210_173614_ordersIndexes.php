<?php

use yii\db\Migration;

class m210210_173614_ordersIndexes extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $this->execute('create index orders_services_id_fk on orders (service_id);');
        $this->execute('create index orders_users_id_fk_i on orders (user_id);');
    }

    public function safeDown()
    {
        $this->execute('drop index orders_services_id_fk_i on orders;');
        $this->execute('drop index orders_users_id_fk_i on orders;');

    }
}
