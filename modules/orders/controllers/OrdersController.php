<?php
/**
 *
 */
declare(strict_types=1);

namespace app\modules\orders\controllers;

use app\modules\orders\models\OrdersSearch;
use app\modules\orders\models\Orders;

use app\modules\orders\models\ServicesSearch;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\Request;


/**
 * Class OrdersController
 *
 * @package app\modules\orders\controllers
 */
class OrdersController extends Controller
{
    /**
     * Вывод таблицы заказов
     *
     * @param Request $request
     * @return string
     */
    public function actionIndex(Request $request): string
    {
        $ordersSearch = new OrdersSearch();

        return $this->render(
            'index',
            [
                'ordersSearch' => $ordersSearch,
                'ordersDataProvider' => $ordersSearch->search($request->getQueryParams())->getDataProvider(),
                'statuses' => Orders::STATUSES,
                'mods' => Orders::MODS,
                'services' => ServicesSearch::getCountedByOrders()
            ]
        );
    }

    /**
     * Экспорт заказов в csv в потоке
     *
     * @param Request $request
     * @throws Exception
     */
    public function actionExport(Request $request): void
    {
        $exporter = (new OrdersSearch())->search($request->getQueryParams())->getCsvExporter();
        $exporter
            ->setResourceAddress(
                'php://output',
                'orders_' . date('Y-m-d_h-i-s') . '.csv')
            ->export();
    }
}
