<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=db-container;dbname=yii-example',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',

    'enableQueryCache'=> true,
    'queryCacheDuration' => 3600,
];
